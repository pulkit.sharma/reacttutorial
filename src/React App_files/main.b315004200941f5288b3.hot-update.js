webpackHotUpdate("main",{

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.css */ "./src/index.css");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_index_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _new_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./new.png */ "./src/new.png");
/* harmony import */ var _new_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_new_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
var _jsxFileName = "/home/vecto/my-app/src/index.js";








class FirstComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    const techCompanies = [{
      label: "Apple",
      value: 1
    }, {
      label: "Facebook",
      value: 2
    }, {
      label: "Netflix",
      value: 3
    }, {
      label: "Tesla",
      value: 4
    }, {
      label: "Amazon",
      value: 5
    }, {
      label: "Alphabet",
      value: 6
    }];
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "firstComponent",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _new_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: "Smiley face",
      height: "120",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), " "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_6__["default"], {
      options: techCompanies,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }))));
  }

}

class SecondComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  installation() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Use as little or as much React as you need.!</h2></center><br><br>React has been designed from the start for gradual adoption, and you can use as little or as much React as you need. Perhaps you only want to add some “sprinkles of interactivity” to an existing page. React components are a great way to do that.<br/><br/> The majority of websites aren’t, and don’t need to be, single-page apps. With a few lines of code and no build tooling, try React in a small part of your website. You can then either gradually expand its presence, or keep it contained to a few dynamic widgets.<br><br><li>Add React in One Minute</li><li>Optional: Try React with JSX (no bundler necessary!)</li>";
  }

  gettingStarted() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Getting Started!</h2></center><br><br>When starting a React project, a simple HTML page with script tags might still be the best option. It only takes a minute to set up! <br> As your application grows, you might want to consider a more integrated setup. There are several JavaScript toolchains we recommend for larger applications. Each of them can work with little to no configuration and lets you take full advantage of the rich React ecosystem.<br/>People come to React from different backgrounds and with different learning styles. Whether you prefer a more theoretical or a practical approach, we hope you’ll find this section helpful.<br/><br/><li>If you prefer to learn by doing, start with our practical tutorial.</li><li>If you prefer to learn concepts step by step, start with our guide to main concepts.</li>";
  }

  mainConcepts() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Main Concepts!</h2></center><br><br><ul><li>Hello World</li><li>Introducing JSX</li><li>Rendering Elements</li><li>Components</li><li>State & lifecycle</li><li>Handling events</li><li>Conditional Rendering</li><li>Lists and Keys</li><li>Forms</li><li>Lifting State Up</li><li>Composition & Inheritance</li><li>Thinking in React</li>";
  }

  advancedGuides() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Advanced Guides!</h2></center><br><br><li>Accessibility</li><li>Code-Splitting</li><li>Context</li><li>Error Boundries</li><li>Forwarding Refs</li><li>Fragments</li><li>JSX in Depths</li><li>Portals</li><li>Optimizing Performance</li><li>React without ES6</li><li>Reconciliation</li><li>Render Props</li><li>Strict Mode</li>";
  }

  apiReference() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Api Reference!<h2></center><br/><br/><li>React.Component</li><li>ReactDOM</li><li>ReactDOMServer</li><li>DOM Elements</li><li>SyntheticEvent</li><li>Test Utilities</li><li>Shallow Renderer</li><li>Test Renderer</li><li>Glossary</li>";
  }

  hooks() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Hooks Started!<h2></center><br/><br/>You can use any AJAX library you like with React. Some popular ones are Axios, jQuery AJAX, and the browser built-in window.fetch.<br/>You should populate data with AJAX calls in the componentDidMount lifecycle method.<br/><br/>This is so you can use setState to update your component when the data is retrieved.<br><br>There are several ways to make sure functions have access to component attributes like this.props and this.state, depending on which syntax and build steps you are using.";
  }

  contributing() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Contribution Started!<h2></center><br/><br/>React is one of Facebook’s first open source projects that is both under very active development and is also being used to ship code to everybody on facebook.com.<br/> We’re still working out the kinks to make contributing to this project as easy and transparent as possible, but we’re not quite there yet.<br/> Hopefully this document makes the process for contributing clear and answers some questions that you may have.<br/>Facebook has adopted a Code of Conduct that we expect project participants to adhere to. Please read the full text so that you can understand what actions will and will not be tolerated. ";
  }

  faq() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Faq Started!</h2></center><br/><br/>setState() schedules an update to a component’s state object. When state changes, the component responds by re-rendering.<br/>props (short for “properties”) and state are both plain JavaScript objects.<br/>While both hold information that influences the output of render, they are different in one important way: props get passed to the component (similar to function parameters) whereas state is managed within the component (similar to variables declared within a function).<br/>Calls to setState are asynchronous - don’t rely on this.state to reflect the new value immediately after calling setState. Pass an updater function instead of an object if you need to compute values based on the current state (see below for details).";
  }

  home() {
    document.getElementById("third").innerHTML = "";
    document.getElementById("third").innerHTML = "<center><h2>Declarative</h2><br/></center><h3>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.Declarative views make your code more predictable and easier to debug.</h3> <br/> <center><h2>Component-Based</h2><br/></center><h3>Build encapsulated components that manage their own state, then compose them to make complex UIs.Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.</h3>";
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "SecondComponent",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.home,
      id: "homee",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, "HOME"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.installation,
      id: "install",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, "INSTALLATION"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.gettingStarted,
      id: "gettingStarted",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, "Getting Started"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.mainConcepts,
      id: "mainConcepts",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }, "MAIN CONCEPTS"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.advancedGuides,
      id: "advancedGuides",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, "ADVANCED GUIDES"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.apiReference,
      id: "apiReference",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, "API REFERENCE"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.hooks,
      id: "hooks",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, "HOOKS"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.contributing,
      id: "contributing",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, "CONTRIBUTING"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#",
      onClick: this.faq,
      id: "faq",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, "FAQ")));
  }

}

class ThirdComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "ThirdComponent",
      id: "third",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, "Declarative"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, "React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes. Declarative views make your code more predictable and easier to debug."), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, "Component-Based"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }, "Build encapsulated components that manage their own state, then compose them to make complex UIs. Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM."));
  }

}

class Main extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "some intial value!"
    };
    this.updateState = this.updateState.bind(this);
    console.log("constructor!");
  }

  updateState(event) {
    this.setState({
      data: "a new value!"
    }); //console.log(event.target.type);
  }

  render() {
    const greeting1 = "hey Component 1";
    const greeting2 = "hey Component 2";
    const greeting3 = "hey Component 3";
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(FirstComponent, {
      name: greeting1,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(SecondComponent, {
      name: greeting2,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 163
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ThirdComponent, {
      name: greeting3,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165
      },
      __self: this
    }), "  */}");
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentWillMount() {
    console.log("componentWillMount");
  }

}

react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Main, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 184
  },
  __self: undefined
}), document.getElementById('root')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_5__["unregister"]();

/***/ })

})
//# sourceMappingURL=main.b315004200941f5288b3.hot-update.js.map