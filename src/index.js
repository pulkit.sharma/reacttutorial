/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import logo from './new.png';
import signUp from './signUp.gif';
import * as serviceWorker from './serviceWorker';
import Select from 'react-select';
import ReactSearchBox from 'react-search-box';
import bgVdo from './bgVdo.mp4';
//import 'bootstrap/dist/css/bootstrap.min.css';

    class FirstComponent extends React.Component
    {
      constructor(props)
      {
        super(props);
        this.state={
          userName:'',
          password:''
        }
      }
      updateDropDown = (event) =>
      {
        var currentValue=event.value;
        console.log(event.value);
        this.props.updateDropDown(currentValue);
      }

      showForm=()=>
      {
        document.getElementById("third").innerHTML = "";
        document.getElementById("third").innerHTML="<center><h2>CONTACT US</h2><br/><br/><form><label>Username</label><input type=\"text\" id=\"user1\" /><br/><br><label>Password</label>&nbsp;<input type=\"password\" id=\"pass1\"/><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type=\"submit\" class=\"fcSub\">Submit</button></form></center>";
      }

      handleUsername=(event)=>
      {
      this.setState({
        userName: event.value
      })
      }

      handlePassword=(event)=>
      {
        this.setState({
          password: event.value
      })
      }

      updateLoginFlag = () =>
      {
        var loginFlag=false;
        this.props.updateLoginFlag(loginFlag);
      }

      render() {
      const techCompanies = [
        { label: "Basics", value: 1 },
        { label: "Advance", value: 2 }
      ];
      
      const data = [
        {
          key: 'john',
          value: 'John Doe',
        },
        {
          key: 'jane',
          value: 'Jane Doe',
        },
        {
          key: 'mary',
          value: 'Mary Phillips',
        },
        {
          key: 'robert',
          value: 'Robert',
        },
        {
          key: 'karius',
          value: 'Karius',
        },
      ];
     
      return (
          <div className="firstComponent">
          <div className="flex-container">
            <div style={{"flexGrow": 1 }}><Select options={ techCompanies } className="fcs1" onChange={(e) => this.updateDropDown(e)}/></div>    
            <div style={{"flexGrow": 1}}><a href="#" className="fcb1"><span>About Us</span></a></div>
            <div style={{"flexGrow": 1}}><a href="#" className="fcb2"><span>Feeds</span></a></div>
            <div style={{"flexGrow": 2}}><div className="container" style={{"left":"50px"}}><img src={logo} className="fcimg1" alt="Smiley face"/><div className="overlay"><div className="text"><a href="https://reactjs.org/" style={{"textDecoration": "none","color":"white"}}>React.org</a></div></div> </div></div>
            <div style={{"flexGrow": 1}}><a href="#" className="fcb4" onClick={this.showForm}><span>Contact Us</span></a></div>
            <div style={{"flexGrow": 1}}><a href="#" className="fcb3" onClick={this.updateLoginFlag}><span>Logout</span></a></div>
            <div style={{"flexGrow": 1,"height":"55px","width":"80px"}}><ReactSearchBox className="fcSearch" placeholder="Search..." value="Doe" data={data} callback={record => console.log(record)}/></div>   
          </div>    
          </div>
            );
      }

    }
    class SecondComponent extends React.Component
    {
      
    installation ()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Use as little or as much React as you need.!</h2></center><br><br>React has been designed from the start for gradual adoption, and you can use as little or as much React as you need. Perhaps you only want to add some “sprinkles of interactivity” to an existing page. React components are a great way to do that.<br/><br/> The majority of websites aren’t, and don’t need to be, single-page apps. With a few lines of code and no build tooling, try React in a small part of your website. You can then either gradually expand its presence, or keep it contained to a few dynamic widgets.<br><br><li>Add React in One Minute</li><li>Optional: Try React with JSX (no bundler necessary!)</li>";
    }
    gettingStarted()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Getting Started!</h2></center><br><br>When starting a React project, a simple HTML page with script tags might still be the best option. It only takes a minute to set up! <br> As your application grows, you might want to consider a more integrated setup. There are several JavaScript toolchains we recommend for larger applications. Each of them can work with little to no configuration and lets you take full advantage of the rich React ecosystem.<br/>People come to React from different backgrounds and with different learning styles. Whether you prefer a more theoretical or a practical approach, we hope you’ll find this section helpful.<br/><br/><li>If you prefer to learn by doing, start with our practical tutorial.</li><li>If you prefer to learn concepts step by step, start with our guide to main concepts.</li>";
    }
    mainConcepts()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Main Concepts!</h2></center><br><br><ul><li>Hello World</li><li>Introducing JSX</li><li>Rendering Elements</li><li>Components</li><li>State & lifecycle</li><li>Handling events</li><li>Conditional Rendering</li><li>Lists and Keys</li><li>Forms</li><li>Lifting State Up</li><li>Composition & Inheritance</li><li>Thinking in React</li>";
    }
    advancedGuides()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Advanced Guides!</h2></center><br><br><li>Accessibility</li><li>Code-Splitting</li><li>Context</li><li>Error Boundries</li><li>Forwarding Refs</li><li>Fragments</li><li>JSX in Depths</li><li>Portals</li><li>Optimizing Performance</li><li>React without ES6</li><li>Reconciliation</li><li>Render Props</li><li>Strict Mode</li>";
    }
    apiReference()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Api Reference!<h2></center><br/><br/><li>React.Component</li><li>ReactDOM</li><li>ReactDOMServer</li><li>DOM Elements</li><li>SyntheticEvent</li><li>Test Utilities</li><li>Shallow Renderer</li><li>Test Renderer</li><li>Glossary</li>";
    }
    hooks()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Hooks Started!<h2></center><br/><br/>You can use any AJAX library you like with React. Some popular ones are Axios, jQuery AJAX, and the browser built-in window.fetch.<br/>You should populate data with AJAX calls in the componentDidMount lifecycle method.<br/><br/>This is so you can use setState to update your component when the data is retrieved.<br><br>There are several ways to make sure functions have access to component attributes like this.props and this.state, depending on which syntax and build steps you are using.";
    }
    contributing()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Contribution Started!<h2></center><br/><br/>React is one of Facebook’s first open source projects that is both under very active development and is also being used to ship code to everybody on facebook.com.<br/> We’re still working out the kinks to make contributing to this project as easy and transparent as possible, but we’re not quite there yet.<br/> Hopefully this document makes the process for contributing clear and answers some questions that you may have.<br/>Facebook has adopted a Code of Conduct that we expect project participants to adhere to. Please read the full text so that you can understand what actions will and will not be tolerated. ";
    }

    faq()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML="<center><h2>Faq Started!</h2></center><br/><br/>setState() schedules an update to a component’s state object. When state changes, the component responds by re-rendering.<br/>props (short for “properties”) and state are both plain JavaScript objects.<br/>While both hold information that influences the output of render, they are different in one important way: props get passed to the component (similar to function parameters) whereas state is managed within the component (similar to variables declared within a function).<br/>Calls to setState are asynchronous - don’t rely on this.state to reflect the new value immediately after calling setState. Pass an updater function instead of an object if you need to compute values based on the current state (see below for details).";
    }

    home()
    {
      document.getElementById("third").innerHTML = "";
      document.getElementById("third").innerHTML= "<center><h2>Declarative</h2><br/></center><h3>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.Declarative views make your code more predictable and easier to debug.</h3> <br/> <center><h2>Component-Based</h2><br/></center><h3>Build encapsulated components that manage their own state, then compose them to make complex UIs.Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.</h3>";
    }
    
      render() {
        return (
          <div className="SecondComponent">
            {
            this.props.currentValue === 1 ? 
            <center>         
                    <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">HOME</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.installation} id="installation">INSTALLATION</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.gettingStarted} id="gettingStarted">GETTING STARTED</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.mainConcepts} id="mainConcepts">MAIN CONCEPTS</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.advancedGuides} id="advancedGuides">ADVANCED GUIDES</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.apiReference} id="apiReference">API REFERENCE</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.hooks} id="hooks">HOOKS</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.contributing} id="contributing">CONTRIBUTING</a>
                    <br/><br/> <a  href="#" className="scb1" onClick={this.faq} id="faq">FAQ</a>
             </center> : 
             <center>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">ACCESSIBILITY</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">CONTEXT</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">FRAGMENTS</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">PORTALS</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">RECONCILIATION</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">RENDER PROPS</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">ERROR BOUNDARIES</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">CODE-SPLITTING</a>
             <br/><br/> <a  href="#" className="scb1" onClick={this.home} id="home">STRICT MODE</a>
             </center>
            }
      
            </div>
        );
    }
    }

    class ThirdComponent extends React.Component
    {
      
      constructor(props)
      {
        super(props);
        this.state={
          userName:''
        }
      }
     
      render()
      {
        console.log('p1',this.props.contactClick);
        return(
          <div className="ThirdComponent" id="third">

          <center>
          <h2>Declarative</h2><br/>
          </center>
          <h3>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.
          Declarative views make your code more predictable and easier to debug.</h3> <br/>
          <center>
          <h2>Component-Based</h2><br/>
          </center>
          <h3>
          Build encapsulated components that manage their own state, then compose them to make complex UIs.
          Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.
          </h3>
          
          </div>
        );
      }
    }

    class FourthComponent extends React.Component
    {
      render()
      {
      return(
        <div className="fourthComponent" id="fourth">
          <center style={{"margin":"-20px"}}><h5 style={{"color":"white"}}>Powered by VS</h5></center> 
        </div>
      );
      }
    } 

    class LoginComponent extends React.Component
    {
      
      login=()=>
      {
        var loginFlag=this.props.loginFlag;
        var user2=document.getElementById("user2").value;
        var pass2=document.getElementById("pass2").value;
        var users = JSON.parse(localStorage.getItem("users"));
        console.log(users);
         for(var i=0;i<users.length;i++)
         {
           if(users[i].username===user2 && users[i].password===pass2)
           {
            loginFlag=true;
            console.log(loginFlag);
            this.updateLoginFlag(loginFlag);
            break;
           }
         }
      }
      updateLoginFlag = (loginFlag) =>
      {
        console.log(loginFlag);
        this.props.updateLoginFlag(loginFlag);
      }
      render()
      {
        return(
            <div className="loginComponent" id="loginComp"> 
            <center>
              <br/><br/><br></br>
            <h2>LOGIN</h2>
           
            <br/><br/>
            <form>
            <label>Username</label>&emsp;&emsp;
            <input type="text" id="user2" required/>
            <br/><br/>
            <label>Password</label>&nbsp;&emsp;&emsp;
            <input type="password" id="pass2" required/>
            <br/><br/>
     
            <button type="submit" className="fcSub" onClick={this.login}>Login</button>
            </form>
            </center>
            </div>
        );
      }
    }
    class SignUpComponent extends React.Component
    {
      validate=()=>{
        var username= document.getElementById("user1").value;
        var pass= document.getElementById("pass1").value;
        if(username===""||pass==="")
        {
          alert("Please fill up the form!");
        }
        else
        {
          this.Register();
        }
      }
      
      Register=()=>
      {
        
        var username= document.getElementById("user1").value;
        var pass= document.getElementById("pass1").value;
        var contact= document.getElementById("contact1").value;
        var address= document.getElementById("address1").value;
        
       // var users=[];
        var userList = JSON.parse(localStorage.getItem("users"));
        var user1={username:username,password:pass,contact:contact,address:address};
        userList.push(user1);
        // userList.push(user1);
         localStorage.setItem("users",JSON.stringify(userList));
        // console.log(userList);

        document.getElementById("user1").value="";
        document.getElementById("pass1").value="";
        document.getElementById("contact1").value="";
        document.getElementById("address1").value="";
        alert("Registration successful!");
      }
      render()
      {
        return(
          
          <div className="SignUpComponent" id="SignUpComp">
            
            <center>
            <br/><br/><br></br> 
            <h2>SIGN UP</h2>
           
            <br/><br/>
            <form>
            <label>Username</label>&emsp;&emsp;&emsp;
            <input type="text" id="user1" required/>
            <br/><br/>
            <label>Password</label>&emsp;&nbsp;&emsp;&emsp;
            <input type="password" id="pass1" required/>
            <br/><br/>
            <label>Contact Number</label>&nbsp;
            <input type="text" id="contact1" required/>
            <br/><br/>
            <label>Address</label>&nbsp;&emsp;&emsp;&emsp;&emsp;
            <input type="text" id="address1" required/>
            <br/><br/>
            
            &emsp;<button type="submit" className="fcSub" onClick={this.validate}>Register</button>
            </form>
            </center>
          </div>
        );
      }
    }
    class Header extends React.Component
    {
      render()
      {
        return(

            <div className="HeaderComponent" id="HeaderComp">
            {/* <video className='videoTag' autoPlay loop muted>
            <source src={bgVdo} className="bgVdo" type='video/mp4' />
            </video> */}
            <br/><br/> <center style={{"margin":"-20px"}}><h1 style={{"color":"white"}}>Welcome to React.org</h1></center>
            </div>
        );
      }
    }

    class Main extends React.Component {
      constructor(props)
      {
        super(props);
        this.state=
        {
         currentValue:1,
         loginFlag:false
        }
        console.log("constructor!");
      }
     
      updateDropDown=(currentValue)=>{  
             
        this.setState({currentValue:currentValue});
      }
      updateContactUs=(contactClick)=>{

        this.setState({contactClick:contactClick});
      }
      updateLoginFlag=(loginFlag)=>
      {
        this.setState({loginFlag:loginFlag});
      }
      render () {
  
        return (
          <div className="main">
            { this.state.loginFlag === true    ? 

             <div> <FirstComponent updateDropDown={this.updateDropDown} currentValue={this.state.currentValue} updateLoginFlag={this.updateLoginFlag} loginFlag={this.state.loginFlag}/>

             <SecondComponent currentValue={this.state.currentValue} />

             <ThirdComponent/>

             <FourthComponent/></div>
             : 
             <div>
               
             <Header/>
             <LoginComponent updateLoginFlag={this.updateLoginFlag} loginFlag={this.state.loginFlag}/>
             <div className = "vertical"></div> 
             <SignUpComponent/>
             </div>
            }  
          </div>
        )
      }

      componentDidMount()
      {
        console.log("componentDidMount");
      }
      componentWillMount()
      {
        console.log("componentWillMount");
      }
    }

ReactDOM.render(<Main/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
